<div>
    <h2>Tambah Data Cast</h2>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nama Cast</label><br>
            <input type="text" class="form-control" name="name" id="name" placeholder="Nama cast" required>
            @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label><br>
            <input type="number" class="form-control" name="umur" id="umur" required>
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>