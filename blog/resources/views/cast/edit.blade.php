<div>
    <h2>Edit Cast Info {{$cast->id}}</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama Cast</label><br>
            <input type="text" class="form-control" name="name" id="name" value="{{$cast->name}}" placeholder="Nama cast" required>
            @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label><br>
            <input type="number" class="form-control" name="umur" id="umur" value="{{$cast->umur}}" required>
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10" value="{{$cast->bio}}"></textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>