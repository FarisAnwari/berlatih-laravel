<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//testing
Route::get('/', function () {
    return view('index');
});

Route::get('/test', function () {
    return "OK test is working";
});

Route::get('/halo/{name?}', function ($name = "User") {
    return "yahallo $name-kun";
});

Route::get('/pangkat2/{num?}', function ($num = 2) {
    return $num * $num;
})->where('num', '[0-9]+');
//end testing

//tugas templating
Route::get('/form', 'HomeController@form');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/data', function () {
    return view('table.data');
});

Route::get('/data-tables', function () {
    return view('table.data-tables');
});
//end tugas templating

//tugas crud laravel
//1. view all cast
// Route::get('/cast', 'CastController@index');
// //2. view form that allows insertion of new cast data
// Route::get('/cast/create', 'CastController@create');
// //3. insert aforementioned data into Cast table
// Route::post('/cast', 'CastController@store');
// //4. show specific cast's data
// Route::get('/cast/{cast_id}', 'CastController@show');
// //5. view form that edits preexisting cast's info
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// //6. update cast's data with previous input
// Route::put('/cast/{cast_id}', 'CastController@update');
// //7. delete cast tuple
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::resource('cast', 'CastController');
