<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('peran', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            $table->unsignedInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->string('nama', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
    }
}
