<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Cast;

class CastController extends Controller
{
    //
    public function index()
    {
        // $cast = DB::table('cast')->get();
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }


    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // dd('validasi berhasil uwu');
        // $query = DB::table('cast')->insert([
        //     "name" => $request["name"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        $cast = new Cast;
        $cast->name = $request->name;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->save();
        return redirect('/cast');
    }

    public function show($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // dd('validasi berhasil uwu');
        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         "name" => $request["name"],
        //         "umur" => $request["umur"],
        //         "bio" => $request["bio"]
        //     ]);
        $cast = Cast::find($id);
        $cast->name = $request->name;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast');
    }

    public function destroy($id)
    {
        // $query = DB::table('cast')->where('id', $id)->delete();
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
